import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe("App.isFizz", () => {
  test("it should take an integer as a parameter and return true if the number is divisible by 3", () => {
      expect(App.isFizz(3)).toBe(true);
      expect(App.isFizz(6)).toBe(true);
      expect(App.isFizz(9)).toBe(true)
  });

  test('it should take an integer as a parameter and return false if the interger provided is not divisible by 3', () => {
    expect(App.isFizz(0)).toBe(false);
    expect(App.isFizz(1)).toBe(false);
    expect(App.isFizz(2)).toBe(false);
    expect(App.isFizz(4)).toBe(false);
    expect(App.isFizz(5)).toBe(false);
    expect(App.isFizz(7)).toBe(false);
  });

  test('it should throw an error if the input is a string', () => {
    expect(() => {App.isFizz("three")}).toThrow("Invalid input");
  });

  test('it should be able to take intergers as strings', () => {
    expect(App.isFizz("3")).toBe(true);
    expect(App.isFizz("4")).toBe(false);
  });

  test('it should throw an error if the input is null or undefined', () => {
    expect(() => App.isFizz(null)).toThrow("Invalid input");
    expect(() => App.isFizz(undefined)).toThrow("Invalid input");
  });
});

describe("App.isBuzz", () => {
  test("it should take an integer as a parameter and return true if the number is divisible by 5", () => {
      expect(App.isBuzz(5)).toBe(true);
      expect(App.isBuzz(10)).toBe(true);
      expect(App.isBuzz(15)).toBe(true)
  });

  test('it should take an integer as a parameter and return false if the interger provided is not divisible by 3', () => {
    expect(App.isBuzz(0)).toBe(false);
    expect(App.isBuzz(1)).toBe(false);
    expect(App.isBuzz(2)).toBe(false);
    expect(App.isBuzz(3)).toBe(false);
    expect(App.isBuzz(4)).toBe(false);
    expect(App.isBuzz(6)).toBe(false);
  });

  test('it should throw an error if the input is a string', () => {
    expect(() => {App.isBuzz("five")}).toThrow("Invalid input");
  });

  test('it should be able to take intergers as strings', () => {
    expect(App.isBuzz("5")).toBe(true);
    expect(App.isBuzz("2")).toBe(false);
  });

  test('it should throw an error if the input is null or undefined', () => {
    expect(() => App.isBuzz(null)).toThrow("Invalid input");
    expect(() => App.isBuzz(undefined)).toThrow("Invalid input");
  });
});

describe('App.isWednesday', () => {
  test('it should accept a function (Date.getDay()) as an argument if the function returns 3 it should then return true', () => {
    // Date.getDay() returns 0 to 6 = sunday to saturday wednesday === 3
    const myMockGetDay = () => {
      return 3;
    }
    expect(App.isWednesday(myMockGetDay())).toBe(true);
  });
  test('it should return false if the callback function returns anything other than 3', () => {
    const myMockGetDay2 = () => {
      return 1;
    }
    expect(App.isWednesday(myMockGetDay2())).toBe(false)
  });
});

describe('App.createArray', () => {
  test('it should accept an integer and return an array of all the integers from 1 to the given integer', () => {
    expect(App.createArray(10)).toEqual([1,2,3,4,5,6,7,8,9]);
  });
  test('it should throw an error if the value passed in is not a number', () => {
    expect(() => App.createArray("ten")).toThrow("Invalid input");
  });
  test('it should be able to accept integers as strings', () => {
    expect(App.createArray("10")).toEqual([1,2,3,4,5,6,7,8,9]);
  });
  test('it should throw an error if the number is negative', () => {
    expect(() => App.createArray(-10)).toThrow("Invalid input");
  });
});

describe("App.getNext20", () => {
  test('it should take an currentIndex and an Array and return the following 20 items', () => {
    const myArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40];
    const currentIndex = 5;
    expect(App.getNext20(currentIndex, myArray)).toEqual({values: [6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25], index: 26});
  });
  test('if the array has less than 20 items remaining it should return the remaining items only', () => {
    const myShortArray = [0,1,2,3,4,5,6,7,8,9,10,11,12];
    expect(App.getNext20(0, myShortArray)).toEqual({values: [1,2,3,4,5,6,7,8,9,10,11,12], index: 13});
  })
});

describe("App.getPrev20", () => {
  test('it should take an currentIndex and an Array and return the following 20 items', () => {
    const myArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40];
    const currentIndex = 26;
    expect(App.getPrev20(currentIndex, myArray)).toEqual({values: [6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25], index: 6});
  });
  test('if the array has less than 20 items remaining it should return the remaining items only', () => {
    const myShortArray = [0,1,2,3,4,5,6,7,8,9,10,11,12];
    expect(App.getPrev20(13, myShortArray)).toEqual({values: [1,2,3,4,5,6,7,8,9,10,11,12], index: 0});
  })
});
