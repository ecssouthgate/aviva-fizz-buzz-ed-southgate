import React, { Component } from 'react';
import './App.css';

const UserInput = (props) => {
  return(
    <div className="container">
      <form className="userInput">
        <label>
          Enter a positive integer
          <input type="text" value={props.input} onChange={props.handleChange} />
        </label>
      </form>
    </div>
  )
}

const NumberList = (props) => {
    let arrayToRender = App.createArray(props.input);
    let numberList = [];
    for(let i = 0; i < arrayToRender.length; i++) {
      numberList.push(
        <Num  value={arrayToRender[i]}
              isFizz={props.isFizz}
              isBuzz={props.isBuzz}
              isWednesday={props.isWednesday}
              key={"num" + i}/>
      );
    }
    return(<ul style={{listStyle: "none"}}>{numberList}</ul>);
}

const Num = (props) => {
  const now = new Date(Date.now());
  var day = now.getDay();
  if( App.isFizz(props.value) && App.isBuzz(props.value) && App.isWednesday(day) ){
    return(
      <li key={props.value}>
        <span style={{color: "blue"}}>Wizz</span><span style={{color: "green"}}>Wuzz</span>
      </li>
    );
  } else if ( App.isFizz(props.value) && App.isWednesday(day) ) {
    return(
      <li key={props.value} style={{color: "blue"}}>
        Wizz
      </li>
    );
  } else if ( App.isBuzz(props.value) && App.isWednesday(day) ) {
    return(
      <li key={props.value} style={{color: "green"}}>
        Wuzz
      </li>
    );
  } else if ( App.isFizz(props.value) && App.isBuzz(props.value) ) {
    return(
      <li key={props.value}>
        <span style={{color: "blue"}}>Fizz</span><span style={{color: "green"}}>Buzz</span>
      </li>
    );
  } else if( App.isFizz(props.value) ) {
    return(
      <li key={props.value} style={{color: "blue"}}>
        Fizz
      </li>
    );
  } else if( App.isBuzz(props.value) ) {
    return(
      <li key={props.value} style={{color: "green"}}>
        Buzz
      </li>
    );
  } else {
    return(
      <li key={props.value}>
        {props.value}
      </li>
    );
  }
}

const ErrorPane = (props) => {
  return(
    <div>
      <h2>Oops something went wrong</h2>
      <p>{props.errorMessage}</p>
      <img style={{width: "600px"}}src="https://media1.popsugar-assets.com/files/thumbor/EYb5dO2AAuFKts5Vj6o8wUPLV_E/fit-in/1024x1024/filters:format_auto-!!-:strip_icc-!!-/2014/08/08/878/n/1922507/caef16ec354ca23b_thumb_temp_cover_file32304521407524949/i/Funny-Cat-GIFs.jpg" />
    </div>
  );
}

class App extends Component {
  state = { error: false,
            input: 1,
            currentIndex: 1,
            arrayToRender: [],
            errorMessage: ""}

  static isFizz = (n) => {
    n = parseInt(n);
    if(Number.isNaN(n)){
      throw new Error("Invalid input");
    } else if (n === 0) {
      return false;
    } else if(n % 3 === 0) {
      return true;
    } else {
      return false;
    }
  }

  static isBuzz = (n) => {
    n = parseInt(n);
    if(Number.isNaN(n)){
      throw new Error("Invalid input");
    } else if (n === 0) {
      return false;
    } else if(n % 5 === 0) {
      return true;
    } else {
      return false;
    }
  }

  static isWednesday = (fn) => {
    if(fn === 3) {
      return true;
    } else {
      return false;
    }

  }

  static createArray = (n) => {
    n = parseInt(n);
    if(Number.isNaN(n) || n <= 0 ){
      throw new Error("Invalid input");
    } else {
      let newArray = [];
      for(let i = 1; i < n; i ++) {
        newArray.push(i);
      }
      return newArray;
    }
  }

  static getNext20 = (index, array) => {
    const next20 = [];
    let currentIndex = index + 1;
    const finalIndex = currentIndex + 20;
    for(let i = currentIndex; i < finalIndex; i++) {
      if(array[i]) {
        next20.push(array[i]);
        currentIndex ++
      }
    }
    return {values: next20, index: currentIndex}
  }

  static getPrev20 = (index, array) => {
    const prev20 = [];
    let currentIndex = index;
    let startIndex = currentIndex - 20;
    if(startIndex < 0) {
      startIndex = 0;
      currentIndex += 1;
    }
    for(let i = startIndex; i < currentIndex; i ++) {
      if(array[i]) {
        prev20.push(array[i]);
      }
    }
    return {values: prev20, index: startIndex}
  }

  handleChange = (event) => {
    if(event.target.value <= 0) {
      this.setState({input: 1, error: true, errorMessage: "Input cannot be less than or equal to 0, any decimal place values will be ignored."});
    } else if (event.target.value == 1) {
      this.setState({input:1, error: true, errorMessage: "Input is exactly 1 so no numbers will be displayed...here's a cat..."})
    } else if (event.target.value > 1000) {
      this.setState({input:1, error: true, errorMessage: "Input must be between 1 and 1000"})
    } else if(isNaN(parseInt(event.target.value))) {
      this.setState({input: 1, error: true, errorMessage: "Input is not a number"})
    } else {
      this.setState({input: event.target.value, error: false, errorMessage: ""})
    }
  }

  render() {
    if(this.state.error) {
      return(
        <div className="App">
          <UserInput handleChange={this.handleChange} />
          <ErrorPane errorMessage={this.state.errorMessage}/>
        </div>
      )
    } else {
      return (
        <div className="App">
          <UserInput handleChange={this.handleChange} />
          <NumberList isFizz={this.isFizz}
                      isBuzz={this.isBuzz}
                      arrayToRender={this.arrayToRender}
                      isWednesday={this.isWednesday}
                      createArray={this.createArray}
                      input={this.state.input}
                      />
        </div>
    );
  }
  }
}

export default App;
